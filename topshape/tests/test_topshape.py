"""
Unit testing module for the topshape module.
"""
import unittest
import time
try:
    from unittest import mock, TestCase
except ImportError:  # python 2
    import mock
    from unittest import TestCase
from urwid import SimpleListWalker, AttrMap, Columns, ExitMainLoop, Filler, \
    Frame, Text, Edit
from topshape import BodyBox, TopShape, TopShapeError, CacheThread, Header


class TestBodyBox(TestCase):
    """Unit tests for BodyBox class."""
    def setUp(self):
        self.body = BodyBox([{'label': 'column1'}], None)

    def test_init(self):
        """Test BodyBox.__init__()."""
        # columns should not be empty
        with self.assertRaises(TopShapeError):
            BodyBox([], None)

        self.assertIsInstance(self.body, BodyBox)
        self.assertEqual(10, self.body.default_column_size)
        self.assertEqual('center', self.body.default_column_alignment)
        self.assertEqual('desc', self.body.default_column_order)
        self.assertEqual([{'label': 'column1',
                           'size': 10,
                           'alignment': 'center',
                           'order': 'desc'}],
                         self.body.columns)
        self.assertIsNone(self.body.func)

    def test__sort_key(self):
        """Test BodyBox._sort_key()."""
        self.assertEqual('value1', self.body._sort_key(('value1',)))
        self.assertEqual(1, self.body._sort_key(('1',)))
        self.assertEqual(1.0, self.body._sort_key(('1.0',)))

    def test_sorting_column(self):
        """Test BodyBox.sorting_column."""
        self.assertEqual('column1', self.body.sorting_column)
        with self.assertRaises(TopShapeError):
            self.body.sorting_column = 'column2'

        self.body.columns = ({'label': 'column1'}, {'label': 'column2'})
        self.body.sorting_column = 'column2'
        self.assertEqual('column2', self.body.sorting_column)

    def test_column_names(self):
        """Test BodyBox.column_names."""
        self.assertEqual(['column1'], self.body.column_names)

    def test_columns(self):
        """Test BodyBox.columns."""
        expected = [{'label': 'column1', 'size': 10, 'alignment': 'center',
                     'order': 'desc'}]
        self.assertEqual(expected, self.body.columns)

        with self.assertRaises(TopShapeError):
            self.body.columns = [{'size': 10}]

    def test_update(self):
        """Test BodyBox.update()"""
        def test_func():
            yield ('foo',)

        self.body.func = test_func

        self.assertEqual(SimpleListWalker([]), self.body.body)

        self.body.update()
        self.assertIsInstance(self.body.body.contents[0], AttrMap)
        self.assertIsInstance(self.body.body.contents[0].original_widget,
                              Columns)
        self.assertIsInstance(self.body.body.contents[1], Columns)
        self.assertEqual('column1',
                         self.body.body.contents[0].original_widget.contents[0]
                         [0].original_widget.text)

        self.assertEqual('foo', self.body.body.contents[1][0].text)

    def test_move_sort(self):
        """Test BodyBox.move_sort_right() and BodyBox.move_sort_left()"""
        def func():
            yield 'foo'
        self.body.func = func

        self.assertEqual('column1', self.body.sorting_column)

        self.body.columns = ({'label': 'column1'}, {'label': 'column2'})
        self.body.move_sort_right()
        self.assertEqual('column2', self.body.sorting_column)

        self.body.move_sort_right()
        self.assertEqual('column2', self.body.sorting_column)

        self.body.move_sort_left()
        self.assertEqual('column1', self.body.sorting_column)

        self.body.move_sort_left()
        self.assertEqual('column1', self.body.sorting_column)

    def test__filter_matches(self):
        """Test BodyBox._filter_matches()."""
        self.body.filter_regex = '^regex$'

        row = ()
        self.assertFalse(self.body._filter_matches(row))

        row = ((''),)
        self.assertFalse(self.body._filter_matches(row))

        row = (('regex'),)
        self.assertTrue(self.body._filter_matches(row))


class TestTopShape(TestCase):
    """Unit tests for TopShape class."""
    def setUp(self):
        self.body_func = mock.Mock()
        self.header_func = mock.Mock()
        self.app = TopShape.create_app(({'label': 'column1'},),
                                       self.body_func,
                                       self.header_func)

    def test_handle_help(self):
        """Test TopShape.handle('h')."""
        self.app.enter_help = mock.Mock()

        self.app._handle_key('h')
        self.app.enter_help.assert_called_with()

    def test_handle_help_quit_in_help(self):
        """
        Test TopShape.handle('q') and TopShape.handle('esc') while
        help output is displayed.
        """
        self.app.on_help = mock.Mock()
        self.app.exit_help = mock.Mock()

        self.app.on_help.return_value = True
        self.app._handle_key('q')
        self.app.exit_help.assert_called_with()

        self.app.on_help.reset_mock()
        self.app.exit_help.reset_mock()

        self.app.on_help.return_value = True
        self.app._handle_key('esc')
        self.app.exit_help.assert_called_with()

    def test_handle_help_quit_not_in_help(self):
        """
        Test TopShape.handle('q') and TopShape.handle('esc') while
        help output is not displayed.
        """
        self.app.on_help = mock.Mock()
        self.app.exit_help = mock.Mock()
        self.app.key_map = {}

        self.app.on_help.return_value = False
        self.app._handle_key('q')
        self.app.exit_help.assert_not_called()

        self.app.on_help.reset_mock()
        self.app.exit_help.reset_mock()

        self.app.on_help.return_value = False
        self.app._handle_key('esc')
        self.app.exit_help.assert_not_called()

    def test__handle_key_with_input(self):
        """
        Test TopShape.handle('f') where pressing 'f' will cause an input
        request from the user.
        """
        foo = mock.Mock()
        self.app.key_map = {'f': (foo, 'foo')}

        self.assertIsNone(self.app._handle_key('f'))
        foo.assert_not_called()

    def test__handle_key_custom_key(self):
        """Test TopShape._handle_key('f') without an input request"""
        foo = mock.Mock()
        self.app.key_map = {'f': foo}

        self.assertIsNone(self.app._handle_key('f'))
        foo.assert_called_with(self.app)

    def test_create_app_minimal(self):
        """Minimal TopShape.create_app()"""
        self.assertIsInstance(self.app, TopShape)
        self.assertEqual('', self.app.help_text)
        self.assertEqual(2, self.app.refresh_rate)
        self.assertIsInstance(self.app.key_map, dict)
        self.assertEqual(['q'], list(self.app.key_map.keys()))

    def test_create_app_with_footer(self):
        """Test TopShape.create_app() with footer function"""
        body_func = mock.Mock()
        header_func = mock.Mock()
        footer_func = mock.Mock()
        app = TopShape.create_app(({'label': 'column1'},),
                                  body_func,
                                  header_func,
                                  footer_func)
        self.assertIsInstance(app, TopShape)

    def test_exit(self):
        """Test TopShape.exit()"""
        with self.assertRaises(ExitMainLoop):
            TopShape.exit()

    def test_update(self):
        """Test TopShape.update()"""
        self.app.frame = mock.Mock()
        self.app.frame.header = mock.Mock()
        self.app.frame.header.contents = ((mock.Mock(),),)
        self.app.frame.body = mock.Mock()
        self.app.frame.footer = mock.Mock()
        self.app.set_alarm_in = mock.Mock()

        self.assertIsNone(self.app.update())

    def test_enter_help(self):
        """Test TopShape.enter_help()"""
        self.app.on_help = mock.Mock()
        self.app.on_help.return_value = False
        self.app.draw_screen = mock.Mock()

        self.assertIsNone(self.app.enter_help())
        self.app.on_help.assert_called_with()
        self.assertIsInstance(self.app.widget, Filler)

    def test_enter_help_in_help_already(self):
        """Test TopShape.enter_help(), already displaying help"""
        self.app.on_help = mock.Mock()
        self.app.on_help.return_value = True
        self.app.draw_screen = mock.Mock()

        self.assertIsNone(self.app.enter_help())
        self.app.on_help.assert_called_with()
        self.assertIsInstance(self.app.widget, Frame)

    def test_exit_help(self):
        """Test TopShape.exit_help()"""
        self.app.on_help = mock.Mock()
        self.app.on_help.return_value = True
        self.app.draw_screen = mock.Mock()

        self.assertIsNone(self.app.exit_help())
        self.app.on_help.assert_called_with()
        self.assertIsNone(self.app.widget)

    def test_exit_help_not_in_help_already(self):
        """Test TopShape.exit_help(), not displaying help already"""
        self.app.on_help = mock.Mock()
        self.app.on_help.return_value = False
        self.app.draw_screen = mock.Mock()

        self.assertIsNone(self.app.exit_help())
        self.app.on_help.assert_called_with()
        self.assertIsInstance(self.app.widget, Frame)

    def test_on_help(self):
        """Test TopShape.on_help()"""
        self.app.draw_screen = mock.Mock()

        self.assertFalse(self.app.on_help())

        self.app.exit_help()
        self.assertFalse(self.app.on_help())

        self.app.enter_help()
        self.assertTrue(self.app.on_help())

        self.app.enter_help()
        self.assertTrue(self.app.on_help())

        self.app.exit_help()
        self.assertFalse(self.app.on_help())

    def test_move_sort_right(self):
        """Test TopShape.move_sort_right()"""
        self.app.on_help = mock.Mock()
        self.app.on_help.return_value = False
        self.app.widget = mock.Mock()
        self.app.widget.body = mock.Mock()

        self.assertIsNone(self.app.move_sort_right())
        self.app.widget.body.move_sort_right.assert_called_with()

    def test_move_sort_right_on_help(self):
        """Test TopShape.move_sort_right() while on help screen"""
        self.app.on_help = mock.Mock()
        self.app.on_help.return_value = True
        self.app.widget = mock.Mock()
        self.app.widget.body = mock.Mock()

        self.assertIsNone(self.app.move_sort_right())
        self.app.widget.body.move_sort_right.assert_not_called()

    def test_move_sort_left(self):
        """Test TopShape.move_sort_left()"""
        self.app.on_help = mock.Mock()
        self.app.on_help.return_value = False
        self.app.widget = mock.Mock()
        self.app.widget.body = mock.Mock()

        self.assertIsNone(self.app.move_sort_left())
        self.app.widget.body.move_sort_left.assert_called_with()

    def test_move_sort_left_on_help(self):
        """Test TopShape.move_sort_right() while on help screen"""
        self.app.on_help = mock.Mock()
        self.app.on_help.return_value = True
        self.app.widget = mock.Mock()
        self.app.widget.body = mock.Mock()

        self.assertIsNone(self.app.move_sort_left())
        self.app.widget.body.move_sort_left.assert_not_called()

    def test_run(self):
        """Test TopShape.run()."""
        self.app._cache_thread = mock.Mock()
        self.app._cache_thread.body = []
        self.app.update = mock.Mock(side_effect=Exception())

        with self.assertRaises(Exception):
            self.app.run()

        self.app._cache_thread.start.assert_called_with()


class TestCacheThread(TestCase):
    """Unit tests for CacheThread class."""
    def setUp(self):
        self.header_func = mock.Mock()
        self.header_func.return_value = 'header'
        self.footer_func = mock.Mock()
        self.footer_func.return_value = 'footer'
        self.body_func = mock.Mock()
        self.body_func.return_value = []

        self.thread = CacheThread(self.header_func,
                                  self.body_func,
                                  self.footer_func,
                                  0.1)

    def test_start(self):
        """Test CacheThread.start()."""
        self.thread.daemon = True
        self.thread.start()
        while not self.thread.ran_once:
            time.sleep(0.1)

        self.assertEqual('header', self.thread.header)
        self.assertEqual('footer', self.thread.footer)
        self.assertEqual([], self.thread.body)
        self.assertEqual(0.1, self.thread.refresh_rate)
        self.body_func.assert_called_with()
        self.header_func.assert_called_with()
        self.footer_func.assert_called_with()

    def test_start_no_footer(self):
        """Test CacheThread.start() with no footer."""
        header_func = mock.Mock()
        header_func.return_value = 'header'
        body_func = mock.Mock()
        body_func.return_value = []

        thread = CacheThread(header_func, body_func, None, 0.1)
        thread.daemon = True
        thread.start()
        while not thread.ran_once:
            time.sleep(0.1)
            self.assertTrue(thread.is_alive())


class TestHeader(TestCase):
    """Unit tests for Header class."""
    def setUp(self):
        header_text = Text('')
        self.header = Header((('pack', header_text), ('pack', Text(''))))
        self.header.app = mock.Mock()

    def test_keypress_not_enter(self):
        """Test Header.keypress() when key is not enter"""
        self.assertEqual('i', (self.header.keypress((None, None), 'i')))

    def test_keypress_enter(self):
        """Test Header.keypress() when key is enter"""
        self.header.contents[1] = (Edit(''), ('pack', None))
        self.assertIsNone(self.header.keypress((None, None), 'enter'))


if __name__ == '__main__':
    unittest.main()
