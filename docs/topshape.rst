topshape package
================

Subpackages
-----------

.. toctree::

    topshape.tests

Submodules
----------

topshape.topshape module
------------------------

.. automodule:: topshape.topshape
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: topshape
    :members:
    :undoc-members:
    :show-inheritance:
